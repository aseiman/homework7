import java.math.BigInteger;
import java.util.*;

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {
	
   /* REFERENCES:
    * https://www.siggraph.org/education/materials/HyperGraph/video/mpeg/mpegfaq/huffman_tutorial.html
    */
	 
   byte[] orig;
   HashMap<Byte, Integer> freqTable;
   HashMap<Byte, String> encodeTable;
   Node tree;

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {
      this.orig = original;
      createFrequencyHashMap();	 
      createHuffmanTree(); 
      createEncodeTable();      
   }
 
   /** Length of encoded data in bits. 
    * @return number of bits
    */
   public int bitLength() {   	
   	if (this.tree.isLeaf()) {
   		return this.tree.freq;
   	} else {
   		return bitLength(this.tree);
   	}
   }

   /** Length of encoded data in bits. 
    * @param node a top tree node of Huffman tree
    * @return number of bits
    */   
   public int bitLength(Node node) {
   	if (node.isLeaf()) {
   		return node.freq * node.string.length(); 
   	} else {
   		return bitLength(node.left) + bitLength(node.right);
   	} 
   }  
   
   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      StringBuffer bitString = new StringBuffer();
   	for (int i=0; i<origData.length; i++) {
      	bitString.append(this.encodeTable.get(origData[i]));
      }
   	
   	// REFERENCE: http://stackoverflow.com/a/17727713   	
   	return new BigInteger(bitString.toString(), 2).toByteArray();
   }
   
   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {   
   	   	
   	StringBuffer sb = new StringBuffer();
   	for (int i=0; i<encodedData.length; i++) {
   		//REFERENCE: http://stackoverflow.com/a/12310078
   		sb.append(Integer.toBinaryString((encodedData[i] & 0xFF) + 0x100).substring(1));   		
   	}
   	
   	String string = sb.toString();
   	string = string.substring(string.length() - bitLength());
   	
   	List<Byte> byteList = new ArrayList<Byte>();
   	boolean newByte = true;
   	Node node = null;
   	
   	if (this.tree.isLeaf()) {
   		for (int i=0; i<string.length(); i++) {
   			byteList.add(this.tree.data);   			
   		}
   	} else {
   		for (int i=0; i<string.length(); i++) {
   			if (newByte) {
   				newByte = false;
   				node = this.tree;   			
   			} 
   			
   			if (string.charAt(i) == '0') {
   				node = node.left; 
   			} else if (string.charAt(i) == '1') {
   				node = node.right;
   			}
   			
   			if (node.isLeaf()) {
   				byteList.add(node.data);
   				newByte = true;
   			}
   			
   		}   		
   	}
   	
		byte[] decodedBytes = new byte[byteList.size()];
		for (int i=0; i<byteList.size(); i++) {
			decodedBytes[i] = byteList.get(i);
		}
		
      return decodedBytes;
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAAAAAAAAAAABBBBBBCCCDDEEF";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
      // TODO!!! Your tests here!
   }
   
   /** Creates HashMap with bytes and corresponding frequencies.  
    */   
   private void createFrequencyHashMap() {
      HashMap<Byte, Integer> freqTable = new HashMap<Byte, Integer>();
      for (byte b: this.orig) {
         if (!freqTable.containsKey(b)) {
            freqTable.put(b,  1);
         } else {
            freqTable.put(b,  (int)freqTable.get(b) + 1);
         }
      }
      this.freqTable = freqTable;
   }
   
   /** Sorts bytes according to corresponding frequencies. 
    * @return list of bytes
    */
   private List<Byte> sortKeys(HashMap<Byte, Integer> inMap) {
   	List<Byte> byteList = new ArrayList<Byte>();
   	List<Integer> intList = new ArrayList<Integer>();
   	
   	for (Byte b: this.freqTable.keySet()) {
   		byteList.add(b);
   		intList.add(this.freqTable.get(b));
   	}
   	binaryInsertionSort(intList, byteList, 0, intList.size());
   	   	
	   return byteList;
   }
   
   /** Creates Huffman tree.  
    */ 
   private void createHuffmanTree() {
	   // Sort frequency table  	
   	List<Byte> keys = sortKeys(this.freqTable);	   
	   	  
	   // Create nodes for tree
	   List<Node> treeNodes = new ArrayList<Node>();
	   for (int i=0; i<keys.size(); i++) {
		   treeNodes.add(new Node(
				   (byte)keys.get(i),
				   (int)freqTable.get(keys.get(i)),
				   null,
				   null
				   ));
	   }
	   
	   // Arranges nodes into tree
	   while (treeNodes.size() > 1) {
		   Node nodeRight = treeNodes.get(0);
		   treeNodes.remove(0);

		   Node nodeLeft = treeNodes.get(0);		   
		   treeNodes.remove(0);
		   
		   int tempValue = nodeRight.freq + nodeLeft.freq;
		   Node newNode = new Node("*".getBytes()[0], tempValue, nodeLeft, nodeRight);
		   
		   if (treeNodes.size() == 0) {
		   	treeNodes.add(0, newNode);
		   } else if (tempValue < treeNodes.get(0).freq) {
		   	treeNodes.add(0, newNode);		   
		   } else if (tempValue > treeNodes.get(treeNodes.size() - 1).freq) {
		   	treeNodes.add(newNode);
		   } else {
		   	int j = 0;		   	
	   		while (tempValue > treeNodes.get(j).freq && j < treeNodes.size()) {
	   			j++;
	   		}		   	
		   	treeNodes.add(j, newNode);
		   }
	   }	   
	   this.tree = treeNodes.get(0);
   }
       
   /** Creates HashMap with bytes and corresponding encoding bits
    * expressed as string.  
    */ 
   private void createEncodeTable() {
   	this.encodeTable = new HashMap<Byte, String>(); 
   	if (this.tree.isLeaf()) {
   		this.encodeTable.put(this.tree.data, "1");
   	} else {
   		createEncodeTable(this.tree);
   	}   	   	   	  
   }   
   
   /** Creates HashMap with bytes and corresponding encoding bits
    * expressed as string.  
    * @param node a top node of Huffman tree.
    */ 
   private void createEncodeTable(Node node) {   	   	
   	if (!node.isLeaf()) {
   		node.left.string = node.string + "0";  
   		createEncodeTable(node.left);
   		
   		node.right.string = node.string + "1";
   		createEncodeTable(node.right);
   	} else {
   		this.encodeTable.put(node.data, node.string);
   	}
   }
   
   /** Sorts list according to values in other list.  
    * @param listValues a list of values used for sorting
    * @param listSort a list of data to be sorted
    * @param left index of leftmost element to be sorted
    * @param right index of rightmost element to be sorted
    */    
   private void binaryInsertionSort(List<Integer> listValues, List<Byte> listSort, int left, int right) {
   	
   	// Algoritm on v�etud teisest kodut��st ja mugandatud kahe massiivi sorteerimiseks.
   	
		// Katkesta, kui massiiv v6i sorditav massiivi osa liiga lyhikesed
		if (listValues.size() < 2) return;
		if ((right - left) < 2) return;
		
		// K2ib l2bi k6ik positsioonid, alates teisest
		for (int i = left + 1; i < right; i++) {
			
			// V6tab v6rreldava objekti
			int b = listValues.remove(i);
			Byte bside = listSort.remove(i);
			
			// Seab binaarse otsingu ala ja leiab esialgse keskkoha 
			int ileft = left;
			int iright = i;// i, aga mitte i - 1, sest While loopi tingimustes on (ileft < iright)
			int j = (ileft + iright) / 2;
			
			while (ileft < iright) {
				if (listValues.get(j).compareTo(b) <= 0) {
					// Kui suurem v6i v6rdne (stabiilsuse tagamiseks), siis nihutab vasakut piiri
					ileft = j + 1;
				} else if (listValues.get(j).compareTo(b) > 0){
					// Kui v2iksem, siis nihutab paremat piiri
					iright = j;
				}
				// Uuendab keskkohta (ja otsingu tulemust) 
				j = (ileft + iright) / 2;
			}
			
			// Paneb objekti korrektsesse kohta tagasi
			listValues.add(j, b);
			listSort.add(j, bside);
		}		
	}
   
   /** Tree node class
    */ 
   private class Node implements Comparable<Node> {
   	private byte data;
   	private int freq;
   	private Node left, right;
   	private String string;
   	
   	Node(byte data, int freq, Node left, Node right) {
   		this.data = data;
   		this.freq = freq;
   		this.left = left;
   		this.right = right;
   		this.string = "";
   	}		
   	
   	public boolean isLeaf() {
   		if (this.left == null && this.right == null) {
   			return true;    		  
   		} else {
   			return false;
   		} 
   	}
   	
   	public int compareTo(Node that) {
   		return this.freq - that.freq;
   	}
   	
   	public void printMe() {
   		System.out.println(Byte.toString(this.data) + " (f: " + this.freq + ") '" + this.string + "'");
   		if (!this.isLeaf()) {
   			System.out.print("LEFT:");
   			this.left.printMe();
   			System.out.print("RIGHT:");
   			this.right.printMe();
   		}
   	}
   }
}